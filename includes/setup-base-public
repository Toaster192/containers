#ifdef _BASE_IMAGE
ARG BASE_IMAGE=_BASE_IMAGE
#else
ARG BASE_IMAGE=registry.fedoraproject.org/fedora
#endif
#ifdef _BASE_IMAGE_TAG
ARG BASE_IMAGE_TAG=_BASE_IMAGE_TAG
#else
ARG BASE_IMAGE_TAG=34
#endif

FROM $BASE_IMAGE:$BASE_IMAGE_TAG
LABEL maintainer="CKI Project Team <cki-project@redhat.com>"

ARG IMAGE_NAME=_IMAGE_NAME
LABEL name=$IMAGE_NAME

/* Make sure that if a | fails, the build fail */
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

#include "envvars"
#include "certs"

/* Configure dnf */
RUN echo "fastestmirror=true" >> /etc/dnf/dnf.conf
RUN echo "install_weak_deps=false" >> /etc/dnf/dnf.conf
RUN echo "best=true" >> /etc/dnf/dnf.conf

/* gnome-keyring uses file capabilities which breaks buildah */
RUN echo "exclude=gnome-keyring" >> /etc/dnf/dnf.conf

/* Pin shadow-utils due to breakage on podman.
   cpio: cap_set_file failed - Inappropriate ioctl for device */
#ifdef _ELN
RUN dnf -y install python3-dnf-plugin-versionlock
#elif _RAWHIDE
RUN dnf -y install --enablerepo=rawhide --releasever=34 dnf-plugin-versionlock
#else
RUN dnf -y install dnf-plugin-versionlock
#endif
RUN dnf versionlock add shadow-utils

/* Upgrade all packages */
RUN dnf -y upgrade

/* Make RPM happy with arbitrary UID/GID in OpenShift */
RUN chmod g=u /etc/passwd /etc/group

/* Minimal Python 3 stack able to get pip packages from git. */
RUN dnf -y install python3 python3-pip git-core

/* allow basic process inspection. */
RUN dnf -y install procps

/* krbV.Krb5Error: (-1765328160, 'Configuration file does not specify default realm') */
RUN sed -i 's@.*default_realm.*@    default_realm = REDHAT.COM@g' /etc/krb5.conf

/* Handle krbV Invalid UID in persistent keyring name */
RUN sed -i 's@.*default_ccache_name@    # default_ccache_name@g' /etc/krb5.conf
